#slackapp-user-admin

API to admin slack app dynamodb tables.

##Deploy

##Before deploy

```Need to upload the labada code to s3Bucket as zip file and specify the s3 name and file name of the code as parameter.```

```aws cloudformation deploy --template-file cf.yaml --stack-name slackapp-user-admin --capabilities CAPABILITY_IAM --parameter-overrides tableUserIp=CMPP-nonprod-slack-commands-tableUserIp-MH6E110NRQEU tableTeamUsers=CMPP-nonprod-slack-commands-tableTeamUsers-GVXAQ273G2G0 s3Name={Bucket name} fileName={Zip file name}```


##API userip

###Getting all the userip records:

```GET http://{hostname}/stage/userip```

###Getting a userip record based on slackUserId

```GET http://{hostname}/stage/userip/{slackUserId}```

###Getting records based on slackUserName

```GET http://{hostname}/stage/userip?user=biju```

###Create/Update a record

```POST http://{hostname}/stage/userip```

####Payload sample

```{
            "AwsRoles": [
                "ab-poc-readonly",
                "bb-prod-readonly"
            ],
            "IpAddress": "111.233.84.146/32",
            "DefaultAwsRole": "ab-dev-readonly",
            "SlackUserId": "XXXXXX",
            "SlackUserName": "biju-ramachandran",
            "WhitelistTeam":[
                "cmppdev"
            ]
}```

###Delete a record

####Data will be deleted from userIp table 

```DELETE http://{hostname}/stage/userip/{slackUserId}```

####Delete user from related tables

```DELETE http://{hostname}/stage/userip/{slackUserId}?deleteRelated=true```





##API teamusers

###Getting all the teamusers records:

```GET http://{hostname}/stage/teamusers```

###Getting a teamusers record based on WhitelistTeam

```GET http://{hostname}/stage/teamusers/{WhitelistTeam}```

###Create/Update a record

```POST http://{hostname}/stage/teamusers```

####Payload sample

```{
            "Users":  [
                "test1",
                "test2"
                ],
            "WhitelistTeam" : "test"
}```


