var AWS = require('aws-sdk');
var DynamoDB = new AWS.DynamoDB.DocumentClient();
let lambda = new AWS.Lambda();

function return_response(body, callback) {
    var response = {
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        "statusCode": 200,
        "body": body,
        "isBase64Encoded": false
    };
    callback(null, response);
}

function get_all_record(tableName, callback) {
    console.log("Get all function");
    var params = {
        TableName: tableName
    };

    DynamoDB.scan(params, function(err, data) {
        if (err) {
            console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
            return_response(JSON.stringify(err), callback);
        } else {
            console.log("Query succeeded.");
            return_response(JSON.stringify(data), callback);
        }
    });
}


function put_slackId_to_teamUser(payload, callback) {
    console.log("Team :", payload.WhitelistTeam)
    payload.WhitelistTeam.forEach(function(team) {
        var params = {
            TableName: process.env.tableTeamUsers,
            Key: {
                WhitelistTeam: team
            }
        };
        DynamoDB.get(params, function(err, data) {
            if (err) {
                console.log(err);
                return_response(JSON.stringify(err), callback);
            } else {
                var Users = [];
                if (isEmptyObject(data)) {
                    return_response("No data Found", callback);
                } else {
                    console.log("userIp data :", data)
                    if (data.Item.Users) {
                        Users = data.Item.Users.values;
                        console.log("user to push :", Users)
                        Users.push(payload['SlackUserId']);
                    } else {
                        Users.push(payload['SlackUserId']);
                    }
                    var params = {
                        TableName: process.env.tableTeamUsers,
                        Key: {
                            WhitelistTeam: team
                        },
                        UpdateExpression: "SET #u = :label",
                        ExpressionAttributeNames: { '#u': 'Users' },
                        ExpressionAttributeValues: {
                            ":label": DynamoDB.createSet(Users)
                        }
                    };

                    DynamoDB.update(params, function(err, data) {
                        if (err) {
                            console.log("error : " + err);
                            return_response(JSON.stringify(err), callback);
                        } else {
                            console.log("successful Update done in teamUser");
                            return_response(JSON.stringify(payload), callback)
                        }
                    });
                }
            }
        });
    });
}




function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}

function arrayRemove(arr, value) {
    return arr.filter(function(ele) {
        return ele != value;
    });
}

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

exports.handler = (event, context, callback) => {
    console.log("Event" + JSON.stringify(event));

    //userIp table API'S

    if (event.resource == '/userip' || event.resource == '/userip/{id}') {
        console.log("resource : " + "/userip");
        console.log("body: " + JSON.stringify(event.body));
        var tableName = process.env.tableUserIp;
        console.log("Method :" + event.httpMethod);
        switch (event.httpMethod) {
            case 'POST':
                {
                    if (isJson(event.body) == false) {
                        return_response(JSON.stringify({ "error": "Invalid JSON" }), callback);
                    } else {
                        var payload = JSON.parse(event.body);
                        var date = new Date(Date.now()).toLocaleString();
                        console.log("starting PUT execution");
                        console.log("Event" + JSON.stringify(event));
                        console.log("Payload wrap" + JSON.stringify(AWS.DynamoDB.Converter.marshall(payload)));
                        console.log("Payload" + JSON.stringify(payload));
                        if (!payload['SlackUserId']) return_response(JSON.stringify({ "error": "SlackUserId not provided" }), callback);
                        var params = {
                            TableName: tableName,
                            Item: {
                                SlackUserId: payload['SlackUserId'],
                                UpdateTime: date
                            }
                        };
                        if (payload['SlackUserName'] != "") params.Item["SlackUserName"] = payload['SlackUserName'];
                        if (payload['DefaultAwsRole'] != "") params.Item["DefaultAwsRole"] = payload['DefaultAwsRole'];
                        if (payload['IpAddress'] != "") params.Item["IpAddress"] = payload['IpAddress'];
                        if (payload.AwsRoles != "") params.Item["AwsRoles"] = DynamoDB.createSet(payload.AwsRoles);
                        console.log("params : " + JSON.stringify(params));
                        DynamoDB.put(params, function(err, data) {
                            if (err) {
                                console.log("error msg : " + err);
                                callback(return_response(JSON.stringify(err), callback));
                            } else {
                                console.log("successful PUT done");
                                if (payload.WhitelistTeam) {
                                    var params = {
                                        TableName: process.env.tableTeamUsers,
                                        FilterExpression: "contains (#uname, :username)",
                                        ExpressionAttributeNames: {
                                            "#uname": "Users"
                                        },
                                        ExpressionAttributeValues: {
                                            ":username": payload['SlackUserId']
                                        }
                                    };
                                    DynamoDB.scan(params, function(err, existingTeamdata) {
                                        if (err) {
                                            console.log(err);
                                            return_response(JSON.stringify(err), callback);
                                        } else {
                                            if (isEmptyObject(existingTeamdata.Items)) {
                                                console.log("No exsisting team data");
                                                put_slackId_to_teamUser(payload, callback);
                                            } else {
                                                var itemsProcessed = 0;
                                                console.log("Data from contanains :", JSON.stringify(existingTeamdata));
                                                existingTeamdata.Items.forEach(function(item, index, array) {
                                                    console.log("team :", item.Users.values)
                                                    var result = arrayRemove(item.Users.values, payload['SlackUserId']);
                                                    var params = {
                                                        TableName: process.env.tableTeamUsers,
                                                        Key: {
                                                            WhitelistTeam: item.WhitelistTeam,
                                                        }
                                                    };
                                                    if (!isEmptyObject(result)) {
                                                        //params.Item["Users"] = DynamoDB.createSet(result);
                                                        params["UpdateExpression"] = "SET #u = :label";
                                                        params["ExpressionAttributeNames"] = { '#u': 'Users' };
                                                        params["ExpressionAttributeValues"] = { ":label": DynamoDB.createSet(result) };
                                                    } else {
                                                        params["UpdateExpression"] = "SET #u = :label";
                                                        params["ExpressionAttributeNames"] = { '#u': 'Users' };
                                                        params["ExpressionAttributeValues"] = { ":label": null };
                                                    }
                                                    console.log("params update :", params)
                                                    DynamoDB.update(params, function(err, data) {
                                                        if (err) {
                                                            console.log("error : " + err);
                                                            return_response(JSON.stringify(err), callback);
                                                        } else {
                                                            console.log("successful PUT done");
                                                            itemsProcessed++;
                                                            if (itemsProcessed === array.length) {
                                                                if (payload.WhitelistTeam != "") put_slackId_to_teamUser(payload, callback);

                                                            }
                                                        }
                                                    });
                                                });
                                            }
                                        }
                                    });

                                }
                                callback(return_response(JSON.stringify(payload), callback));
                            }
                        });

                    }
                }
                break;
            case 'GET':
                {
                    console.log("starting GET execution");

                    //Get All Item Function

                    if (!event.pathParameters && !event.queryStringParameters) {
                        console.log("Get all users");
                        var returData = {
                            "Items": []
                        };
                        var teamUserTable = process.env.tableTeamUsers;
                        var params = {
                            TableName: tableName
                        };
                        var scanObjectPromise = DynamoDB.scan(params).promise();
                        scanObjectPromise.then(function(data) {
                            var itemsProcessed = 0;
                            data.Items.forEach(function(item, index, array) {
                                var WhitelistTeam = [];
                                var SlackUserId = item.SlackUserId;
                                var params = {
                                    TableName: process.env.tableTeamUsers,
                                    FilterExpression: "contains (#uname, :username)",
                                    ExpressionAttributeNames: {
                                        "#uname": "Users"
                                    },
                                    ExpressionAttributeValues: {
                                        ":username": SlackUserId
                                    }
                                };
                                var scanTeamObjectPromise = DynamoDB.scan(params).promise();
                                scanTeamObjectPromise.then(function(teamData) {
                                    teamData.Items.forEach(function(teamItem) {
                                        var teamUsers = String(teamItem["WhitelistTeam"]);
                                        WhitelistTeam.push(teamUsers);
                                    });
                                    console.log("WhitelistTeam :", WhitelistTeam);
                                    if (!isEmptyObject(WhitelistTeam)) item["WhitelistTeam"] = WhitelistTeam;
                                    console.log("New UserIp Item :", JSON.stringify(item));
                                    returData.Items.push(item);
                                    itemsProcessed++;
                                    if (itemsProcessed === array.length) {
                                        return_response(JSON.stringify(returData), callback)
                                    }
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            });
                        }).catch(function(err) {
                            console.log(err);
                        });
                    }

                    //Get Single Item Using SlackId

                    if (event.pathParameters) {
                        var objectId = event.pathParameters.id;
                        console.log("objectId." + objectId);
                        var params = {
                            TableName: tableName,
                            Key: {
                                SlackUserId: objectId
                            }
                        };
                        DynamoDB.get(params, function(err, data) {
                            if (err) {
                                console.log(err);
                                return_response(JSON.stringify(err), callback);
                            } else {
                                if (isEmptyObject(data)) {
                                    return_response(JSON.stringify({
                                        "status": "Item not found"
                                    }), callback);
                                } else {
                                    var WhitelistTeam = [];
                                    console.log("Finding TeamUser")
                                    var params = {
                                        TableName: process.env.tableTeamUsers,
                                        FilterExpression: "contains (#uname, :username)",
                                        ExpressionAttributeNames: {
                                            "#uname": "Users"
                                        },
                                        ExpressionAttributeValues: {
                                            ":username": objectId
                                        }
                                    };
                                    DynamoDB.scan(params, function(err, teamData) {
                                        if (err) {
                                            console.log(err);
                                            return_response(JSON.stringify(err), callback);
                                        } else {
                                            if (isEmptyObject(teamData.Items)) {
                                                return_response(JSON.stringify(data), callback);
                                            } else {
                                                teamData.Items.forEach(function(teamItem) {
                                                    console.log("item :", teamItem["WhitelistTeam"])
                                                    var teamUsers = String(teamItem["WhitelistTeam"]);
                                                    WhitelistTeam.push(teamUsers);
                                                });
                                                data['WhitelistTeam'] = WhitelistTeam;
                                                return_response(JSON.stringify(data), callback);
                                            }
                                        }
                                    });
                                }
                            }
                        });

                    }

                    //Get Items Using SlackUsername

                    if (event.queryStringParameters) {
                        var returData = {
                            "Items": []
                        };
                        var slkUsrName = event.queryStringParameters.user;
                        console.log("slkUsrName." + slkUsrName);
                        var params = {
                            TableName: tableName,
                            FilterExpression: "contains (#uname, :username)",
                            ExpressionAttributeNames: {
                                "#uname": "SlackUserName"
                            },
                            ExpressionAttributeValues: {
                                ":username": slkUsrName
                            }
                        };
                        var scanObjectPromise = DynamoDB.scan(params).promise();
                        scanObjectPromise.then(function(data) {
                            var itemsProcessed = 0;
                            data.Items.forEach(function(item, index, array) {
                                var WhitelistTeam = [];
                                var SlackUserId = item.SlackUserId;
                                var params = {
                                    TableName: process.env.tableTeamUsers,
                                    FilterExpression: "contains (#uname, :username)",
                                    ExpressionAttributeNames: {
                                        "#uname": "Users"
                                    },
                                    ExpressionAttributeValues: {
                                        ":username": SlackUserId
                                    }
                                };
                                var scanTeamObjectPromise = DynamoDB.scan(params).promise();
                                scanTeamObjectPromise.then(function(teamData) {
                                    teamData.Items.forEach(function(teamItem) {
                                        var teamUsers = String(teamItem["WhitelistTeam"]);
                                        WhitelistTeam.push(teamUsers);
                                    });
                                    if (!isEmptyObject(WhitelistTeam)) item["WhitelistTeam"] = WhitelistTeam;
                                    returData.Items.push(item);
                                    itemsProcessed++;
                                    if (itemsProcessed === array.length) {
                                        return_response(JSON.stringify(returData), callback)
                                    }
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            });
                        }).catch(function(err) {
                            console.log(err);
                        });
                    }
                    break;
                }

                //Delete Items also delete teamUser Table Related Data

            case 'DELETE':
                {
                    console.log("starting DELETE execution");

                    if (event.pathParameters && event.httpMethod == 'DELETE') {
                        var objectId = event.pathParameters.id;
                        var params = {
                            TableName: tableName,
                            Key: {
                                SlackUserId: objectId
                            }
                        };
                        DynamoDB.delete(params, function(err, data) {
                            if (err) {
                                console.log(err);
                                return_response(JSON.stringify(err), callback);
                            } else {
                                if (event.queryStringParameters) {
                                    if (event.queryStringParameters.deleteRelated == "true") {
                                        console.log("Finding TeamUser")
                                        var params = {
                                            TableName: process.env.tableTeamUsers,
                                            FilterExpression: "contains (#uname, :username)",
                                            ExpressionAttributeNames: {
                                                "#uname": "Users"
                                            },
                                            ExpressionAttributeValues: {
                                                ":username": objectId
                                            }
                                        };
                                        DynamoDB.scan(params, function(err, data) {
                                            if (err) {
                                                console.log(err);
                                                return_response(JSON.stringify(err), callback);
                                            } else {
                                                if (isEmptyObject(data)) {
                                                    return_response("No data Found", callback);
                                                } else {
                                                    console.log("Data finded :", data)
                                                    data.Items.forEach(function(item) {
                                                        var result = arrayRemove(item.Users.values, objectId);
                                                        item.Users.values = result;
                                                        var params = {
                                                            TableName: process.env.tableTeamUsers,
                                                            Key: {
                                                                WhitelistTeam: item.WhitelistTeam,
                                                            },
                                                        };
                                                        if (!isEmptyObject(item.Users.values)) {
                                                            params["UpdateExpression"] = "SET #u = :label";
                                                            params["ExpressionAttributeNames"] = { '#u': 'Users' };
                                                            params["ExpressionAttributeValues"] = { ":label": DynamoDB.createSet(item.Users.values) };
                                                        } else {
                                                            params["UpdateExpression"] = "SET #u = :label";
                                                            params["ExpressionAttributeNames"] = { '#u': 'Users' };
                                                            params["ExpressionAttributeValues"] = { ":label": null };
                                                        }
                                                        console.log("params  :", params)
                                                        DynamoDB.update(params, function(err, data) {
                                                            if (err) {
                                                                console.log("error : " + err);
                                                                return_response(JSON.stringify(err), callback);
                                                            } else {
                                                                console.log("successful PUT done");
                                                            }
                                                        });
                                                    });
                                                }
                                            }
                                        });
                                        return_response(JSON.stringify({
                                            "success": "Item deleted"
                                        }), callback);
                                    } else {
                                        return_response(JSON.stringify({
                                            "success": "Item deleted from userIp"
                                        }), callback);
                                    }
                                } else {
                                    return_response(JSON.stringify({
                                        "success": "Item deleted from userIp"
                                    }), callback);
                                }

                            }
                        });
                    }
                    break;
                }

        }
    }


    //Table teamUser API'S

    if (event.resource == '/teamusers' || event.resource == '/teamusers/{id}') {
        console.log("Resource : /teamusers")
        var tableName = process.env.tableTeamUsers;

        console.log("Method :" + event.httpMethod);
        switch (event.httpMethod) {
            case 'POST':
                {
                    if (isJson(event.body) == false) {
                        return_response(JSON.stringify({ "error": "Invalid JSON" }), callback);
                    } else {
                        var payload = JSON.parse(event.body);
                        var date = new Date(Date.now()).toLocaleString();
                        console.log("starting PUT execution");
                        console.log("Event" + JSON.stringify(event));
                        console.log("Payload wrap" + JSON.stringify(AWS.DynamoDB.Converter.marshall(payload)));
                        console.log("Payload" + JSON.stringify(payload));
                        if (!payload['WhitelistTeam']) return_response(JSON.stringify({ "error": "WhitelistTeam not provided" }), callback);
                        var params = {
                            TableName: tableName,
                            Key: {
                                WhitelistTeam: payload['WhitelistTeam']
                            }
                        };
                        DynamoDB.get(params, function(err, data) {
                            if (err) {
                                console.log(err);
                                return_response(JSON.stringify(err), callback);
                            } else {
                                if (isEmptyObject(data)) {
                                    //new data
                                    console.log("New WhitelistTeam")
                                    var params = {
                                        TableName: tableName,
                                        Item: {
                                            WhitelistTeam: payload['WhitelistTeam']
                                        }
                                    };
                                    if (payload['Users'] != "" && payload['Users']) params.Item["Users"] = DynamoDB.createSet(payload['Users']);
                                    DynamoDB.put(params, function(err, data) {
                                        if (err) {
                                            console.log("error : " + err);
                                            return_response(JSON.stringify(err), callback);
                                        } else {
                                            console.log("successful PUT done");
                                            return_response("Success", callback);
                                        }
                                    });
                                    return_response(JSON.stringify(payload), callback);
                                } else {
                                    if (payload['Users'] == "" || !payload['Users']) {
                                        var params = {
                                            TableName: tableName,
                                            Key: {
                                                WhitelistTeam: payload['WhitelistTeam']
                                            },
                                            UpdateExpression: "SET #u = :label",
                                            ExpressionAttributeNames: { '#u': 'Users' },
                                            ExpressionAttributeValues: {
                                                ":label": null
                                            }
                                        };
                                        DynamoDB.update(params, function(err, data) {
                                            if (err) {
                                                console.log("error : " + err);
                                                return_response(JSON.stringify(err), callback);
                                            } else {
                                                console.log("successful PUT done");
                                                return_response("Success", callback);
                                            }
                                        });
                                        return_response(JSON.stringify(payload), callback);
                                    } else {
                                        var params = {
                                            TableName: tableName,
                                            Key: {
                                                WhitelistTeam: payload['WhitelistTeam']
                                            },
                                            UpdateExpression: "SET #u = :label",
                                            ExpressionAttributeNames: { '#u': 'Users' },
                                            ExpressionAttributeValues: {
                                                ":label": DynamoDB.createSet(payload.Users)
                                            }

                                        };
                                        console.log("params", JSON.stringify(params))
                                        DynamoDB.update(params, function(err, data) {
                                            if (err) {
                                                console.log("error : " + err);
                                                return_response(JSON.stringify(err), callback);
                                            } else {
                                                console.log("successful PUT done");
                                                return_response("Success", callback);
                                            }
                                        });
                                        return_response(JSON.stringify(payload), callback);
                                    }

                                }

                            }
                        });

                        break;
                    }
                }

            case 'GET':
                {
                    console.log("starting GET execution");
                    if (!event.pathParameters && !event.queryStringParameters) {
                        //Call Get All Function
                        get_all_record(tableName, callback);
                    }

                    if (event.pathParameters) {
                        var WhitelistTeam = event.pathParameters.id;
                        console.log("WhitelistTeam." + WhitelistTeam);
                        var params = {
                            TableName: tableName,
                            Key: {
                                WhitelistTeam: WhitelistTeam
                            }
                        };
                        DynamoDB.get(params, function(err, data) {
                            if (err) {
                                console.log(err);
                                return_response(JSON.stringify(err), callback);
                            } else {
                                if (isEmptyObject(data)) {
                                    return_response(JSON.stringify({
                                        "status": "Item not found"
                                    }), callback);
                                } else {
                                    return_response(JSON.stringify(data), callback);
                                }

                            }
                        });
                    }

                    break;
                }
        }
    }
};